package co.com.sofka.clean.core.usecase.product;

import co.com.sofka.clean.core.entity.Product;
import co.com.sofka.clean.core.repository.ProductRepository;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Service;

@Service
public class CreateProduct {
    private ProductRepository repository;

    public CreateProduct(ProductRepository repository) {
        this.repository = repository;
    }

    public Publisher<Product> execute(Product product) {
        return this.repository.save(product);
    }
}
