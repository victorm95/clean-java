package co.com.sofka.clean.core.repository;

import co.com.sofka.clean.core.entity.Product;
import org.reactivestreams.Publisher;

public interface ProductRepository {
    Publisher<Product> find();
    Publisher<Product> get(String id);
    Publisher<Product> save(Product product);
    Publisher<Product> update(String id, Product product);
    Publisher<Void> delete(String id);
}
