package co.com.sofka.clean.core.usecase.product;

import co.com.sofka.clean.core.entity.Product;
import co.com.sofka.clean.core.repository.ProductRepository;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Service;

@Service
public class GetProduct {
    private ProductRepository repository;

    public GetProduct(ProductRepository repository) {
        this.repository = repository;
    }

    public Publisher<Product>execute(String id) {
        return this.repository.get(id);
    }
}
