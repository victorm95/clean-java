package co.com.sofka.clean.core.usecase.product;

import co.com.sofka.clean.core.repository.ProductRepository;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Service;

@Service
public class DeleteProduct {
    private ProductRepository repository;

    public DeleteProduct(ProductRepository repository) {
        this.repository = repository;
    }

    public Publisher<Void>execute(String id) {
        return this.repository.delete(id);
    }
}
