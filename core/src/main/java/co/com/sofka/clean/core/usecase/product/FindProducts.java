package co.com.sofka.clean.core.usecase.product;

import co.com.sofka.clean.core.entity.Product;
import co.com.sofka.clean.core.repository.ProductRepository;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Service;

@Service
public class FindProducts {
    private ProductRepository repository;

    public FindProducts(ProductRepository repository) {
        this.repository = repository;
    }

    public Publisher<Product>execute() {
        return this.repository.find();
    }
}
