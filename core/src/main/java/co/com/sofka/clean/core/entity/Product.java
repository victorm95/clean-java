package co.com.sofka.clean.core.entity;

public class Product {
  private String id;
  private String name;
  private double price;

  public Product() { }

  public Product(String id, String name, double price) {
    this.id = id;
    this.name = name;
    this.price = price;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getId() {
    return this.id;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getName() {
    return this.name;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public double getPrice() {
    return this.price;
  }

  @Override
  public String toString() {
    return "Person(:name, :price)"
            .replace(":name", this.name)
            .replace(":price", String.valueOf(this.price));
  }
}
