package co.com.sofka.clean.data.adapter;

import co.com.sofka.clean.data.model.ProductData;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductAdapter extends ReactiveCrudRepository<ProductData, String> {
}
