package co.com.sofka.clean.data.model;

import co.com.sofka.clean.core.entity.Product;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class ProductData extends Product {
    @Id
    private String id;

    public ProductData() {
        super();
    }

    public ProductData(String id, String name, double price) {
        super(null, name, price);
        this.id = id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }
}
