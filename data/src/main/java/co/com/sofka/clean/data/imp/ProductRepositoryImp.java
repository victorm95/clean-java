package co.com.sofka.clean.data.imp;

import co.com.sofka.clean.core.entity.Product;
import co.com.sofka.clean.core.repository.ProductRepository;
import co.com.sofka.clean.data.adapter.ProductAdapter;
import co.com.sofka.clean.data.model.ProductData;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Service;

@Service
public class ProductRepositoryImp implements ProductRepository {
    private ProductAdapter adapter;

    public ProductRepositoryImp(ProductAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public Publisher<Product> find() {
        return this.adapter.findAll().map(product -> new Product(product.getId(), product.getName(), product.getPrice()));
    }

    @Override
    public Publisher<Product>get(String id) {
        return this.adapter.findById(id).map(product -> new Product(product.getId(), product.getName(), product.getPrice()));
    }

    @Override
    public Publisher<Product>save(Product product) {
        ProductData data = new ProductData();
        data.setName(product.getName());
        data.setPrice(product.getPrice());
        return this.adapter.save(data).map(p -> new Product(p.getId(), p.getName(), p.getPrice()));
    }

    @Override
    public Publisher<Product>update(String id, Product product) {
        return this.adapter.findById(id)
                .map(p -> {
                    p.setName(product.getName());
                    p.setPrice(product.getPrice());
                    return p;
                })
                .flatMap(this.adapter::save)
                .map(p -> new Product(p.getId(), p.getName(), p.getPrice()));
    }

    @Override
    public Publisher<Void>delete(String id) {
        return this.adapter.deleteById(id);
    }
}