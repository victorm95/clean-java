package co.com.sofka.clean.server.controller;

import co.com.sofka.clean.core.entity.Product;
import co.com.sofka.clean.core.usecase.product.*;
import org.reactivestreams.Publisher;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.io.PushbackInputStream;

@RestController
@RequestMapping("/products")
public class ProductController {
    private FindProducts findProducts;
    private GetProduct getProduct;
    private CreateProduct createProduct;
    private UpdateProduct updateProduct;
    private DeleteProduct deleteProduct;

    public ProductController(FindProducts findProducts, GetProduct getProduct, CreateProduct createProduct, UpdateProduct updateProduct, DeleteProduct deleteProduct) {
        this.findProducts = findProducts;
        this.getProduct = getProduct;
        this.createProduct = createProduct;
        this.updateProduct = updateProduct;
        this.deleteProduct = deleteProduct;
    }

    @GetMapping
    public Publisher<Product> find() {
        return this.findProducts.execute();
    }

    @GetMapping("/{id}")
    public Publisher<Product> get(@PathVariable("id") String id) {
        return this.getProduct.execute(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Publisher<Product> save(@Valid @RequestBody Product product) {
        return this.createProduct.execute(product);
    }

    @PutMapping("/{id}")
    public Publisher<Product> update(@PathVariable("id") String id, @Valid @RequestBody Product product) {
        return this.updateProduct.execute(id, product);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Publisher<Void> delete(@PathVariable("id") String id) {
        return this.deleteProduct.execute(id);
    }
}
